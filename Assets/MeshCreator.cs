﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;

public class MeshCreator : MonoBehaviour
{
    public Draw drawer;
    MeshFilter meshF;
    Camera cam;
    MeshRenderer rend;
    public ModellingUI ui;
    public Mesh mesh;
    List<MeshGhost> snapshots = new List<MeshGhost>();
    public List<Vector3> selectedVertices = new List<Vector3>();
    public bool alignToGrid;
    public float gridSize = 0.5f;
    public bool allowInput = true;
    enum Action
    {
        None, Grabbing, Rotating, Scaling
    }
    public enum Axis {
        All=0,
        x=1,
        y=2,
        z=4
    }
    Action currentAction;
    public Axis currentAxis;
    Vector3 oldMousePos;
    struct MeshGhost // Why would anybody make vector3s nonserializable? -_- 
    {
        public Vector3[] vertices;
        public int[] triangles;
        public Vector3[] normals;
        public Vector2[] uv;
        public List<Vector3> selectedVertices;
    }
    // Use this for initialization
    void Start()
    {
        meshF = GetComponent<MeshFilter>();
        cam = Camera.main;
        rend = GetComponent<MeshRenderer>();
        mesh = new Mesh();
        mesh.MarkDynamic();
        meshF.mesh = mesh;
    }

    public void GenerateQuad()
    {
        var vertices = new List<Vector3>(mesh.vertices);
        var triangles = new List<int>(mesh.triangles);

        var size = 1f;

        vertices.Add(new Vector3((-size / 2)+drawer.cameraPivot.x - transform.position.x , (-size / 2) + drawer.cameraPivot.y - transform.position.y , 0 + drawer.cameraPivot.z - transform.position.z ));
        vertices.Add(new Vector3((-size / 2) + drawer.cameraPivot.x - transform.position.x , (size / 2) + drawer.cameraPivot.y - transform.position.y , 0 + drawer.cameraPivot.z - transform.position.z ));
        vertices.Add(new Vector3((size / 2) + drawer.cameraPivot.x - transform.position.x , (-size / 2) + drawer.cameraPivot.y - transform.position.y , 0 + drawer.cameraPivot.z - transform.position.z ));
        vertices.Add(new Vector3((size / 2) + drawer.cameraPivot.x - transform.position.x , (-size / 2) + drawer.cameraPivot.y - transform.position.y , 0 + drawer.cameraPivot.z - transform.position.z ));
        vertices.Add(new Vector3((-size / 2) + drawer.cameraPivot.x - transform.position.x , ( size / 2) + drawer.cameraPivot.y - transform.position.y , 0 + drawer.cameraPivot.z - transform.position.z ));
        vertices.Add(new Vector3((size / 2) + drawer.cameraPivot.x - transform.position.x , (size / 2) + drawer.cameraPivot.y - transform.position.y , 0 + drawer.cameraPivot.z - transform.position.z ));

        triangles.Add(vertices.Count-6);
        triangles.Add(vertices.Count - 5);
        triangles.Add(vertices.Count - 4);
        triangles.Add(vertices.Count - 3);
        triangles.Add(vertices.Count - 2);
        triangles.Add(vertices.Count - 1);

        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        RecalculateNormals();
    }
    public void GeneratePlane()
    {
        var vertices = new List<Vector3>(mesh.vertices);
        var triangles = new List<int>(mesh.triangles);

        var size = 2f;

        for (int x = -10; x < 10; x++)
        {
            for (int z = -10; z < 10; z++)
            {
                vertices.Add(new Vector3((size / 2) + x + drawer.cameraPivot.x - transform.position.x, drawer.cameraPivot.y - transform.position.y, (-size / 2) + z + drawer.cameraPivot.z - transform.position.z));
                vertices.Add(new Vector3((-size / 2) +x+ drawer.cameraPivot.x - transform.position.x,  drawer.cameraPivot.y - transform.position.y, (-size / 2) + z + drawer.cameraPivot.z - transform.position.z));
                vertices.Add(new Vector3((-size / 2) + x + drawer.cameraPivot.x - transform.position.x, drawer.cameraPivot.y - transform.position.y, (size / 2) + z + drawer.cameraPivot.z - transform.position.z));
                vertices.Add(new Vector3((size / 2) + x + drawer.cameraPivot.x - transform.position.x, drawer.cameraPivot.y - transform.position.y, (-size / 2) + z + drawer.cameraPivot.z - transform.position.z));
                vertices.Add(new Vector3((-size / 2) + x + drawer.cameraPivot.x - transform.position.x, drawer.cameraPivot.y - transform.position.y, (size / 2) + z + drawer.cameraPivot.z - transform.position.z));
                vertices.Add(new Vector3((size / 2) + x + drawer.cameraPivot.x - transform.position.x, drawer.cameraPivot.y - transform.position.y, (size / 2)+z + drawer.cameraPivot.z - transform.position.z));

                triangles.Add(vertices.Count - 6);
                triangles.Add(vertices.Count - 5);
                triangles.Add(vertices.Count - 4);
                triangles.Add(vertices.Count - 3);
                triangles.Add(vertices.Count - 2);
                triangles.Add(vertices.Count - 1);
            }
        }

        

        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        RecalculateNormals();
    }
    public void GenerateCube()
    {
        var vertices = new List<Vector3>(mesh.vertices);
        var triangles = new List<int>(mesh.triangles);

        var size = 1f;

        vertices.Add(new Vector3((-size / 2) + drawer.cameraPivot.x - transform.position.x, (-size / 2) + drawer.cameraPivot.y - transform.position.y, (-size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((-size / 2) + drawer.cameraPivot.x - transform.position.x, (size / 2) + drawer.cameraPivot.y - transform.position.y, (-size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((size / 2) + drawer.cameraPivot.x - transform.position.x, (-size / 2) + drawer.cameraPivot.y - transform.position.y, (-size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((size / 2) + drawer.cameraPivot.x - transform.position.x, (-size / 2) + drawer.cameraPivot.y - transform.position.y, (-size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((-size / 2) + drawer.cameraPivot.x - transform.position.x, (size / 2) + drawer.cameraPivot.y - transform.position.y, (-size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((size / 2) + drawer.cameraPivot.x - transform.position.x, (size / 2) + drawer.cameraPivot.y - transform.position.y, (-size / 2) + drawer.cameraPivot.z - transform.position.z));

        vertices.Add(new Vector3((-size / 2) + drawer.cameraPivot.x - transform.position.x, (size / 2) + drawer.cameraPivot.y - transform.position.y, (size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((-size / 2) + drawer.cameraPivot.x - transform.position.x, (-size / 2) + drawer.cameraPivot.y - transform.position.y, (size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((size / 2) + drawer.cameraPivot.x - transform.position.x, (-size / 2) + drawer.cameraPivot.y - transform.position.y, (size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((-size / 2) + drawer.cameraPivot.x - transform.position.x, (size / 2) + drawer.cameraPivot.y - transform.position.y, (size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((size / 2) + drawer.cameraPivot.x - transform.position.x, (-size / 2) + drawer.cameraPivot.y - transform.position.y, (size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((size / 2) + drawer.cameraPivot.x - transform.position.x, (size / 2) + drawer.cameraPivot.y - transform.position.y, (size / 2) + drawer.cameraPivot.z - transform.position.z));

        vertices.Add(new Vector3((-size / 2) + drawer.cameraPivot.x - transform.position.x, (-size / 2) + drawer.cameraPivot.y - transform.position.y, (-size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((size / 2) + drawer.cameraPivot.x - transform.position.x, (-size / 2) + drawer.cameraPivot.y - transform.position.y, (-size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((-size / 2) + drawer.cameraPivot.x - transform.position.x, (-size / 2) + drawer.cameraPivot.y - transform.position.y, (size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((-size / 2) + drawer.cameraPivot.x - transform.position.x, (-size / 2) + drawer.cameraPivot.y - transform.position.y, (size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((size / 2) + drawer.cameraPivot.x - transform.position.x, (-size / 2) + drawer.cameraPivot.y - transform.position.y, (-size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((size / 2) + drawer.cameraPivot.x - transform.position.x, (-size / 2) + drawer.cameraPivot.y - transform.position.y, (size / 2) + drawer.cameraPivot.z - transform.position.z));

        vertices.Add(new Vector3((size / 2) + drawer.cameraPivot.x - transform.position.x, (size / 2) + drawer.cameraPivot.y - transform.position.y, (-size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((-size / 2) + drawer.cameraPivot.x - transform.position.x, (size / 2) + drawer.cameraPivot.y - transform.position.y, (-size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((-size / 2) + drawer.cameraPivot.x - transform.position.x, (size / 2) + drawer.cameraPivot.y - transform.position.y, (size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((size / 2) + drawer.cameraPivot.x - transform.position.x, (size / 2) + drawer.cameraPivot.y - transform.position.y, (-size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((-size / 2) + drawer.cameraPivot.x - transform.position.x, (size / 2) + drawer.cameraPivot.y - transform.position.y, (size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((size / 2) + drawer.cameraPivot.x - transform.position.x, (size / 2) + drawer.cameraPivot.y - transform.position.y, (size / 2) + drawer.cameraPivot.z - transform.position.z));
        
        vertices.Add(new Vector3((-size / 2) + drawer.cameraPivot.x - transform.position.x, (-size / 2) + drawer.cameraPivot.y - transform.position.y, (-size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((-size / 2) + drawer.cameraPivot.x - transform.position.x, (-size / 2) + drawer.cameraPivot.y - transform.position.y, (size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((-size / 2) + drawer.cameraPivot.x - transform.position.x, (size / 2) + drawer.cameraPivot.y - transform.position.y, (-size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((-size / 2) + drawer.cameraPivot.x - transform.position.x, (size / 2) + drawer.cameraPivot.y - transform.position.y, (-size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((-size / 2) + drawer.cameraPivot.x - transform.position.x, (-size / 2) + drawer.cameraPivot.y - transform.position.y, (size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((-size / 2) + drawer.cameraPivot.x - transform.position.x, (size / 2) + drawer.cameraPivot.y - transform.position.y, (size / 2) + drawer.cameraPivot.z - transform.position.z));

        vertices.Add(new Vector3((size / 2) + drawer.cameraPivot.x - transform.position.x, (-size / 2) + drawer.cameraPivot.y - transform.position.y, (size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((size / 2) + drawer.cameraPivot.x - transform.position.x, (-size / 2) + drawer.cameraPivot.y - transform.position.y, (-size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((size / 2) + drawer.cameraPivot.x - transform.position.x, (size / 2) + drawer.cameraPivot.y - transform.position.y, (-size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((size / 2) + drawer.cameraPivot.x - transform.position.x, (-size / 2) + drawer.cameraPivot.y - transform.position.y, (size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((size / 2) + drawer.cameraPivot.x - transform.position.x, (size / 2) + drawer.cameraPivot.y - transform.position.y, (-size / 2) + drawer.cameraPivot.z - transform.position.z));
        vertices.Add(new Vector3((size / 2) + drawer.cameraPivot.x - transform.position.x, (size / 2) + drawer.cameraPivot.y - transform.position.y, (size / 2) + drawer.cameraPivot.z - transform.position.z));


        for (int i = 36; i > 0; i--)
        {
            triangles.Add(vertices.Count - i);

        }

        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        RecalculateNormals();
    }

    void Update()
    {
        if (allowInput)
        {
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetMouseButtonDown(0)) Deselect();
            else if (Input.GetMouseButtonDown(0)) Select();
            else if (Input.GetKeyDown(KeyCode.A)) SelectAll();
            else if (Input.GetKeyDown(KeyCode.G)) StartGrab();
            else if (Input.GetMouseButtonDown(1)) RightClick();
            else if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Z)) LoadSnapshot(0);
            else if (Input.GetKeyDown(KeyCode.E)) Extrude();
            else if (Input.GetKeyDown(KeyCode.J)) JoinPoints();
            else if (Input.GetKeyDown(KeyCode.F)) FillFace();
            else if (Input.GetKeyDown(KeyCode.S)) StartScaling();
            else if (Input.GetKeyDown(KeyCode.R)) StartRotating();
            else if (currentAction == Action.None && Input.GetKeyDown(KeyCode.X)) ui.ShowDeleteOptions();
        }
        switch (currentAction)
        {
            case Action.None:
                currentAxis = Axis.All;
                break;
            case Action.Grabbing:
                CheckAxisInput();
                Grab();
                break;
            case Action.Rotating:
                CheckAxisInput();
                Rotate();
                break;
            case Action.Scaling:
                CheckAxisInput();
                Scale();
                break;
            default:
                break;
        }
        if (selectedVertices.Count != 0) {
            ui.ShowVertexCoords(selectedVertices[selectedVertices.Count - 1]);
            
        }
    }
    void LateUpdate()
    {
        oldMousePos = Input.mousePosition;
        oldMousePos.z = 0;
    }

    void CheckAxisInput() {
        if (Input.GetKeyDown(KeyCode.X)){
            if (currentAxis == Axis.All) currentAxis = Axis.x;
            else currentAxis ^= Axis.x;
        }
        if (Input.GetKeyDown(KeyCode.Y))
        {
            if (currentAxis == Axis.All) currentAxis = Axis.y;
            else currentAxis ^= Axis.y;
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (currentAxis == Axis.All) currentAxis = Axis.z;
            else currentAxis ^= Axis.z;
        }
        if ((int)currentAxis == 7) currentAxis = Axis.All;
    }

    void Extrude()
    {
        if (selectedVertices.Count > 1)
        {
            SaveSnapshot();
            UpdateSelectedIndices();
            var vertices = new List<Vector3>(mesh.vertices);
            selectedVertices = new List<Vector3>();
            var triangles = mesh.triangles;
            var triToAdd = new List<int>();

            for (int i = 2; i < triangles.Length; i += 3)
            {
                if (areVerticesSelected[triangles[i - 2]] && areVerticesSelected[triangles[i - 1]])
                {
                    vertices.Add(vertices[triangles[i - 1]]);
                    triToAdd.Add(vertices.Count - 1);
                    vertices.Add(vertices[triangles[i - 2]]);
                    triToAdd.Add(vertices.Count - 1);

                    vertices.Add(vertices[triangles[i - 1]] + new Vector3(0.001f, 0.0001f, 0.001f));
                    triToAdd.Add(vertices.Count - 1);
                    selectedVertices.Add(vertices[vertices.Count - 1]);

                    vertices.Add(vertices[triangles[i - 2]]);
                    triToAdd.Add(vertices.Count - 1);
                    vertices.Add(vertices[triangles[i - 2]] + new Vector3(0.001f, 0.0001f, 0.001f));
                    triToAdd.Add(vertices.Count - 1);
                    selectedVertices.Add(vertices[vertices.Count - 1]);
                    vertices.Add(vertices[triangles[i - 1]] + new Vector3(0.001f, 0.0001f, 0.001f));
                    triToAdd.Add(vertices.Count - 1);
                }
                if (areVerticesSelected[triangles[i - 2]] && areVerticesSelected[triangles[i]])
                {
                    vertices.Add(vertices[triangles[i - 2]]);
                    triToAdd.Add(vertices.Count - 1);
                    vertices.Add(vertices[triangles[i]]);
                    triToAdd.Add(vertices.Count - 1);

                    vertices.Add(vertices[triangles[i - 2]] + new Vector3(0.001f, 0.0001f, 0.001f));
                    triToAdd.Add(vertices.Count - 1);
                    selectedVertices.Add(vertices[vertices.Count - 1]);

                    vertices.Add(vertices[triangles[i]]);
                    triToAdd.Add(vertices.Count - 1);
                    vertices.Add(vertices[triangles[i]] + new Vector3(0.001f, 0.0001f, 0.001f));
                    triToAdd.Add(vertices.Count - 1);
                    selectedVertices.Add(vertices[vertices.Count - 1]);
                    vertices.Add(vertices[triangles[i - 2]] + new Vector3(0.001f, 0.0001f, 0.001f));
                    triToAdd.Add(vertices.Count - 1);
                }
                if (areVerticesSelected[triangles[i - 1]] && areVerticesSelected[triangles[i]])
                {
                    vertices.Add(vertices[triangles[i]]);
                    triToAdd.Add(vertices.Count - 1);
                    vertices.Add(vertices[triangles[i - 1]]);
                    triToAdd.Add(vertices.Count - 1);

                    vertices.Add(vertices[triangles[i]] + new Vector3(0.001f, 0.0001f, 0.001f));
                    triToAdd.Add(vertices.Count - 1);
                    selectedVertices.Add(vertices[vertices.Count - 1]);

                    vertices.Add(vertices[triangles[i - 1]]);
                    triToAdd.Add(vertices.Count - 1);
                    vertices.Add(vertices[triangles[i - 1]] + new Vector3(0.001f, 0.0001f, 0.001f));
                    triToAdd.Add(vertices.Count - 1);
                    selectedVertices.Add(vertices[vertices.Count - 1]);
                    vertices.Add(vertices[triangles[i]] + new Vector3(0.001f, 0.0001f, 0.001f));
                    triToAdd.Add(vertices.Count - 1);
                }
            }
            /*
            for (int i = 2; i < triangles.Length; i += 3)
            {
                for (int x = 0; x < vertices.Count - 1; x++)
                {
                    for (int y = x + 1; y < vertices.Count; y++)
                    {
                        for (int a = 0; a < selectedVerticesIndices[x].Count; a++)
                        {
                            for (int b = 0; b < selectedVerticesIndices[y].Count; b++)
                            {
                                if ((triangles[i] == selectedVerticesIndices[x][a] || triangles[i - 1] == selectedVerticesIndices[x][a] || triangles[i - 2] == selectedVerticesIndices[x][a]) && (triangles[i] == selectedVerticesIndices[y][b] || triangles[i - 1] == selectedVerticesIndices[y][b] || triangles[i - 2] == selectedVerticesIndices[y][b]))
                                {

                                    int tri1 = triangles[i], tri2 = triangles[i - 1], tri3 = triangles[i - 2];
                                    int one = selectedVerticesIndices[x][a], two = selectedVerticesIndices[y][b];
                                    if ((tri1 == one && tri2 == two) || (tri2 == one && tri3 == two) || (tri3 == one && tri1 == two))
                                    {
                                        vertices.Add(vertices[x]);
                                        triToAdd.Add(vertices.Count - 1);
                                        vertices.Add(vertices[y]);
                                        triToAdd.Add(vertices.Count - 1);

                                        vertices.Add(vertices[x] + new Vector3(0.001f, 0.0001f, 0.001f));
                                        triToAdd.Add(vertices.Count - 1);
                                        selectedVertices.Add(vertices[vertices.Count - 1]);

                                        vertices.Add(vertices[y]);
                                        triToAdd.Add(vertices.Count - 1);
                                        vertices.Add(vertices[y] + new Vector3(0.001f, 0.0001f, 0.001f));
                                        triToAdd.Add(vertices.Count - 1);
                                        selectedVertices.Add(vertices[vertices.Count - 1]);
                                        vertices.Add(vertices[x] + new Vector3(0.001f, 0.0001f, 0.001f));
                                        triToAdd.Add(vertices.Count - 1);
                                    }
                                    else {
                                        vertices.Add(vertices[y]);
                                        triToAdd.Add(vertices.Count - 1);
                                        vertices.Add(vertices[x]);
                                        triToAdd.Add(vertices.Count - 1);

                                        vertices.Add(vertices[y] + new Vector3(0.001f, 0.0001f, 0.001f));
                                        triToAdd.Add(vertices.Count - 1);
                                        selectedVertices.Add(vertices[vertices.Count - 1]);

                                        vertices.Add(vertices[x]);
                                        triToAdd.Add(vertices.Count - 1);
                                        vertices.Add(vertices[x] + new Vector3(0.001f, 0.0001f, 0.001f));
                                        triToAdd.Add(vertices.Count - 1);
                                        selectedVertices.Add(vertices[vertices.Count - 1]);
                                        vertices.Add(vertices[y] + new Vector3(0.001f, 0.0001f, 0.001f));
                                        triToAdd.Add(vertices.Count - 1);
                                    }



                                }

                            }
                        }
                    }
                }
            }
            */
        for (int x = 0; x < selectedVertices.Count - 1; x++)
            {
                for (int y = x + 1; y < selectedVertices.Count; y++)
                {
                    if (selectedVertices[x] == selectedVertices[y])
                    {
                        selectedVertices.RemoveAt(y);
                        y--;
                    }
                }
            }

            mesh.vertices = vertices.ToArray();
            triToAdd.AddRange(triangles);
            mesh.triangles = triToAdd.ToArray();
            StartGrab();
        }
    }

    void FillFace()
    {
        if (selectedVertices.Count != 2 && selectedVertices.Count != 3) return;
        SaveSnapshot();
        UpdateSelectedIndices();
        var vertices = new List<Vector3>(mesh.vertices);
        var triangles = new List<int>(mesh.triangles);
        if (selectedVertices.Count == 2)
        {
            vertices.Add(selectedVertices[0]);
            vertices.Add(selectedVertices[1]);
            vertices.Add((selectedVertices[0] + selectedVertices[1]) / 2);
            bool foundTriangle = false;
            for (int i = 2; i < triangles.Count; i += 3)
            {
                for (int x = 0; x < selectedVerticesIndices[0].Count; x++)
                {
                    if (triangles[i - 2] == selectedVerticesIndices[0][x])
                    {
                        for (int y = 0; y < selectedVerticesIndices[1].Count; y++)
                        {
                            if (triangles[i - 1] == selectedVerticesIndices[1][y])
                            {
                                triangles.Add(vertices.Count - 3);
                                triangles.Add(vertices.Count - 1);
                                triangles.Add(vertices.Count - 2);
                                foundTriangle = true;
                            }
                            else if (triangles[i] == selectedVerticesIndices[1][y])
                            {
                                triangles.Add(vertices.Count - 3);
                                triangles.Add(vertices.Count - 2);
                                triangles.Add(vertices.Count - 1);
                                foundTriangle = true;
                            }
                        }
                    }
                    else if (triangles[i - 1] == selectedVerticesIndices[0][x])
                    {
                        for (int y = 0; y < selectedVerticesIndices[1].Count; y++)
                        {
                            if (triangles[i - 2] == selectedVerticesIndices[1][y])
                            {
                                triangles.Add(vertices.Count - 3);
                                triangles.Add(vertices.Count - 2);
                                triangles.Add(vertices.Count - 1);
                                foundTriangle = true;
                            }
                            else if (triangles[i] == selectedVerticesIndices[1][y])
                            {
                                triangles.Add(vertices.Count - 3);
                                triangles.Add(vertices.Count - 1);
                                triangles.Add(vertices.Count - 2);
                                foundTriangle = true;
                            }
                        }
                    }
                    else if (triangles[i] == selectedVerticesIndices[0][x])
                    {
                        for (int y = 0; y < selectedVerticesIndices[1].Count; y++)
                        {
                            if (triangles[i - 2] == selectedVerticesIndices[1][y])
                            {
                                triangles.Add(vertices.Count - 3);
                                triangles.Add(vertices.Count - 1);
                                triangles.Add(vertices.Count - 2);
                                foundTriangle = true;
                            }
                            else if (triangles[i - 1] == selectedVerticesIndices[1][y])
                            {
                                triangles.Add(vertices.Count - 3);
                                triangles.Add(vertices.Count - 2);
                                triangles.Add(vertices.Count - 1);
                                foundTriangle = true;
                            }
                        }
                    }
                }
            }
            if (!foundTriangle)
            {
                triangles.Add(vertices.Count - 3);
                triangles.Add(vertices.Count - 1);
                triangles.Add(vertices.Count - 2);
            }
            mesh.vertices = vertices.ToArray();
            mesh.triangles = triangles.ToArray();
            RecalculateNormals();
            selectedVertices.Clear();
            selectedVertices.Add(vertices[vertices.Count - 1]);
            StartGrab();
        }
        else if (selectedVertices.Count == 3)
        {
            vertices.Add(selectedVertices[0]);
            vertices.Add(selectedVertices[1]);
            vertices.Add(selectedVertices[2]);
            for (int n = 1; n < 3; n++)
            {
                for (int i = 2; i < triangles.Count; i += 3)
                {
                    for (int x = 0; x < selectedVerticesIndices[0].Count; x++)
                    {
                        if (triangles[i - 2] == selectedVerticesIndices[0][x])
                        {
                            for (int y = 0; y < selectedVerticesIndices[1 * n].Count; y++)
                            {
                                if ((triangles[i - 1] == selectedVerticesIndices[1 * n][y]) != (n == 2))
                                {
                                    triangles.Add(vertices.Count - 3);
                                    triangles.Add(vertices.Count - 1);
                                    triangles.Add(vertices.Count - 2);
                                }
                                else if (triangles[i] == selectedVerticesIndices[1 * n][y] != (n == 2))
                                {
                                    triangles.Add(vertices.Count - 3);
                                    triangles.Add(vertices.Count - 2);
                                    triangles.Add(vertices.Count - 1);

                                }
                            }
                        }
                        else if (triangles[i - 1] == selectedVerticesIndices[0][x])
                        {
                            for (int y = 0; y < selectedVerticesIndices[1 * n].Count; y++)
                            {
                                if (triangles[i - 2] == selectedVerticesIndices[1 * n][y] != (n == 2))
                                {
                                    triangles.Add(vertices.Count - 3);
                                    triangles.Add(vertices.Count - 2);
                                    triangles.Add(vertices.Count - 1);

                                }
                                else if (triangles[i] == selectedVerticesIndices[1 * n][y] != (n == 2))
                                {
                                    triangles.Add(vertices.Count - 3);
                                    triangles.Add(vertices.Count - 1);
                                    triangles.Add(vertices.Count - 2);

                                }
                            }
                        }
                        else if (triangles[i] == selectedVerticesIndices[0][x])
                        {
                            for (int y = 0; y < selectedVerticesIndices[1 * n].Count; y++)
                            {
                                if (triangles[i - 2] == selectedVerticesIndices[1 * n][y] != (n == 2))
                                {
                                    triangles.Add(vertices.Count - 3);
                                    triangles.Add(vertices.Count - 1);
                                    triangles.Add(vertices.Count - 2);

                                }
                                else if (triangles[i - 1] == selectedVerticesIndices[1 * n][y] != (n == 2))
                                {
                                    triangles.Add(vertices.Count - 3);
                                    triangles.Add(vertices.Count - 2);
                                    triangles.Add(vertices.Count - 1);

                                }
                            }
                        }
                    }
                }
            }
            mesh.vertices = vertices.ToArray();
            mesh.triangles = triangles.ToArray();
            RecalculateNormals();
        }
    }

    void Deselect()
    {
        currentAction = Action.None;
        var ray = cam.ScreenPointToRay(Input.mousePosition);
        var vertices = mesh.vertices;
        for (int i = 0; i < vertices.Length; i++)
        {
            if (Vector3.Cross(ray.direction, ray.origin - vertices[i] - transform.position).sqrMagnitude / ray.direction.sqrMagnitude < 0.025f)
            { // Distance = (dir X dirPointToOrigin) / dir 
                if (selectedVertices.Contains(vertices[i]))
                    selectedVertices.Remove(vertices[i]);
                break;
            }

        }
    }

    void Select()
    {
        if (currentAction != Action.None) {
            currentAction = Action.None;
            return;
        }
        currentAction = Action.None;
        var ray = cam.ScreenPointToRay(Input.mousePosition);
        var vertices = mesh.vertices;
        for (int i = 0; i < vertices.Length; i++)
        {
            if (Vector3.Cross(ray.direction, ray.origin - vertices[i] - transform.position).sqrMagnitude / ray.direction.sqrMagnitude < 0.025f)
            { // Distance = (dir X dirPointToOrigin) / dir 
                if (!Input.GetKey(KeyCode.LeftShift)) selectedVertices.Clear();
                if (!selectedVertices.Contains(vertices[i]))
                    selectedVertices.Add(vertices[i]);
                break;
            }

        }
    }
    void RightClick()
    {
        if (currentAction != Action.None)
        {
            currentAction = Action.None;
            LoadSnapshot(0);
        }
    }
    void SelectAll()
    {
        var vertices = mesh.vertices;
        if (selectedVertices.Count == 0)
        {
            selectedVertices.Clear();
            for (int i = 0; i < vertices.Length; i++)
            {
                if (!selectedVertices.Contains(vertices[i])) selectedVertices.Add(vertices[i]);
            }
        }
        else {
            selectedVertices.Clear();
        }
    }
    Vector3[] grabPositions;
    void SaveSnapshot()
    {
        var ghost = new MeshGhost() { vertices = mesh.vertices, triangles = mesh.triangles, normals = mesh.normals, uv = mesh.uv, selectedVertices = new List<Vector3>(selectedVertices) };
        snapshots.Add(ghost);
    }
    public void LoadSnapshot(int age)
    {
        if (snapshots.Count > age)
        {
            for (int i = 0; i < age; i++)
            {
                snapshots.RemoveAt(snapshots.Count - 1);
            }
            var ghost = snapshots[snapshots.Count - 1];
            snapshots.RemoveAt(snapshots.Count - 1);
            if (snapshots.Count > 30) snapshots.RemoveRange(0, snapshots.Count - 20);
            try
            {
                mesh.vertices = ghost.vertices;
                mesh.triangles = ghost.triangles;
                mesh.normals = ghost.normals;
                mesh.uv = ghost.uv;
            }
            catch(Exception ex) {
                Debug.Log("Error occured: " + ex);
            }
            selectedVertices = ghost.selectedVertices;
        }
    }
    Vector3 cachedSelectionMidpoint;
    void StartRotating() {
        SaveSnapshot();
        cachedSelectionMidpoint = SelectionMidPoint();
        UpdateSelectedIndices();
        currentAction = Action.Rotating;
    }
    void Rotate() {
        var vertices = mesh.vertices;
        var ray = cam.ScreenPointToRay(oldMousePos);
        ray.origin -= transform.position;
        var d = -(drawer.cameraForward.x * cachedSelectionMidpoint.x + drawer.cameraForward.y * cachedSelectionMidpoint.y + drawer.cameraForward.z * cachedSelectionMidpoint.z);
        var t = -(d + ray.origin.x * drawer.cameraForward.x + ray.origin.y * drawer.cameraForward.y + ray.origin.z * drawer.cameraForward.z)
                /
            (drawer.cameraForward.x * ray.direction.x + drawer.cameraForward.y * ray.direction.y + drawer.cameraForward.z * ray.direction.z);
        var start = new Vector3(ray.origin.x + t * ray.direction.x, ray.origin.y + t * ray.direction.y, ray.origin.z + t * ray.direction.z);

        ray = cam.ScreenPointToRay(Input.mousePosition);
        ray.origin -= transform.position;
        d = -(drawer.cameraForward.x * cachedSelectionMidpoint.x + drawer.cameraForward.y * cachedSelectionMidpoint.y + drawer.cameraForward.z * cachedSelectionMidpoint.z);
        t = -(d + ray.origin.x * drawer.cameraForward.x + ray.origin.y * drawer.cameraForward.y + ray.origin.z * drawer.cameraForward.z)
                /
            (drawer.cameraForward.x * ray.direction.x + drawer.cameraForward.y * ray.direction.y + drawer.cameraForward.z * ray.direction.z);
        var dest = new Vector3(ray.origin.x + t * ray.direction.x, ray.origin.y + t * ray.direction.y, ray.origin.z + t * ray.direction.z);

        var rot = Quaternion.FromToRotation(start - cachedSelectionMidpoint, dest - cachedSelectionMidpoint);
        selectedVertices.Clear();
        for (int i = 0; i < areVerticesSelected.Length; i++)
        {
            if (areVerticesSelected[i]) {
                var v = vertices[i] - cachedSelectionMidpoint;
                vertices[i] = cachedSelectionMidpoint + new Quaternion(currentAxis == Axis.All || (currentAxis & Axis.x) == Axis.x ? rot.x : 0,
                    currentAxis == Axis.All || (currentAxis & Axis.y) == Axis.y ? rot.y : 0,
                    currentAxis == Axis.All || (currentAxis & Axis.z) == Axis.z ? rot.z : 0,
                    rot.w
                    )                *v;
                selectedVertices.Add(vertices[i]);
            }
        }
        mesh.vertices = vertices;
    }

    void StartScaling() {
        SaveSnapshot();
        cachedSelectionMidpoint = SelectionMidPoint();
        UpdateSelectedIndices();
        currentAction = Action.Scaling;
    }

    void Scale() {
        var ray = cam.ScreenPointToRay(oldMousePos);
        ray.origin -= transform.position;
        var d = -(drawer.cameraForward.x * cachedSelectionMidpoint.x + drawer.cameraForward.y * cachedSelectionMidpoint.y + drawer.cameraForward.z * cachedSelectionMidpoint.z);
        var t = -(d + ray.origin.x * drawer.cameraForward.x + ray.origin.y * drawer.cameraForward.y + ray.origin.z * drawer.cameraForward.z)
                /
            (drawer.cameraForward.x * ray.direction.x + drawer.cameraForward.y * ray.direction.y + drawer.cameraForward.z * ray.direction.z);
        var start = new Vector3(ray.origin.x + t * ray.direction.x, ray.origin.y + t * ray.direction.y, ray.origin.z + t * ray.direction.z);
        ray = cam.ScreenPointToRay(Input.mousePosition);
        ray.origin -= transform.position;
        d = -(drawer.cameraForward.x * cachedSelectionMidpoint.x + drawer.cameraForward.y * cachedSelectionMidpoint.y + drawer.cameraForward.z * cachedSelectionMidpoint.z);
        t = -(d + ray.origin.x * drawer.cameraForward.x + ray.origin.y * drawer.cameraForward.y + ray.origin.z * drawer.cameraForward.z)
                /
            (drawer.cameraForward.x * ray.direction.x + drawer.cameraForward.y * ray.direction.y + drawer.cameraForward.z * ray.direction.z);
        var dest = new Vector3(ray.origin.x + t * ray.direction.x, ray.origin.y + t * ray.direction.y, ray.origin.z + t * ray.direction.z);
        var mouseOffset = (dest - start).magnitude;
        var midPointView = cam.WorldToViewportPoint(cachedSelectionMidpoint);
        midPointView.z = 0;
        if (Vector3.Dot(dest - start, dest-cachedSelectionMidpoint) < 0) mouseOffset *= -1;
        //if ((cam.ScreenToViewportPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0)) - midPointView).sqrMagnitude < (cam.ScreenToViewportPoint(oldMousePos) - midPointView).sqrMagnitude) mouseOffset *= -1;
        var vertices = mesh.vertices;
        selectedVertices.Clear();
        for (int i = 0; i < vertices.Length; i++)
        {
            if (areVerticesSelected[i]) {
                var dir = (vertices[i] - cachedSelectionMidpoint);
                var offset = dir * mouseOffset;
                
                vertices[i] = vertices[i] + new Vector3(currentAxis == Axis.All || (currentAxis & Axis.x) == Axis.x ? offset.x : 0,
                     currentAxis == Axis.All || (currentAxis & Axis.y) == Axis.y ? offset.y : 0,
                     currentAxis == Axis.All || (currentAxis & Axis.z) == Axis.z ? offset.z : 0
                     );
                selectedVertices.Add(vertices[i]);
            }
        }
        for (int i = 0; i < selectedVertices.Count; i++)
        {
            for (int y = 0; y < selectedVertices.Count; y++)
            {
                if (i != y && selectedVertices[i] == selectedVertices[y]) selectedVertices.RemoveAt(i>y? i :y);
            }
        }
        mesh.vertices = vertices;
    }
    /*
    public void DeleteTriangles()
    {
        SaveSnapshot();
        var vertices = mesh.vertices;
        var triangles = new List<int>(mesh.triangles);
        var uv = mesh.uv;
        UpdateSelectedIndices();
        int num = 0;
        var removed = new List<int>();
        for (int i = 2; i < triangles.Count;i+=3)
        {
            Debug.Log(i);
            if (areVerticesSelected[triangles[i - 2]] && areVerticesSelected[triangles[i - 1]] && areVerticesSelected[triangles[i]])
            {
                Debug.Log(triangles[i - 2] + ", " + triangles[i - 1] + ", " + triangles[i]);
                vertices[triangles[i - 2]] = new Vector3(999, 989.654321f, 999);
                vertices[triangles[i - 1]] = new Vector3(999, 989.654321f, 999);
                vertices[triangles[i]] = new Vector3(999, 989.654321f, 999);
                removed.Add(triangles[i - 2]);
                removed.Add(triangles[i - 1]);
                removed.Add(triangles[i]);
                triangles[i - 2] = -1;
                triangles[i - 1] = -1;
                triangles[i ] = -1;
                num += 3;
            }
            //else i += 3;
        }
        for (int i = 0; i < triangles.Count; i++)
        {
            for (int x = 0; x < removed.Count; x++)
            {
                if (triangles[i] > removed[x]) triangles[i]--;
            }
            if (triangles[i] < 0) {
                triangles.RemoveAt(i);
                i--;
            }
        }

        var newVerts = new Vector3[vertices.Length - num];
        var newUv = new Vector2[vertices.Length-num];
        for (int i = 0, n = 0; i < vertices.Length; i++)
        {
            if (vertices[i].y != 989.654321f)
            {
                newVerts[n] = vertices[i];
                if (i < uv.Length) newUv[n] = uv[i];
                n++;
            }
        }
        selectedVertices.Clear();
        mesh.triangles = triangles.ToArray();
        mesh.vertices = newVerts;
        mesh.uv = newUv;
        RecalculateNormals();
    }
    */

    
    public void DeleteTriangles() {
        SaveSnapshot();
        var vertices = new List<Vector3>(mesh.vertices);
        var triangles = new List<int>(mesh.triangles);
        var uv = new List<Vector2>(mesh.uv);
        UpdateSelectedIndices();
        var deleted = new List<int>();
        for (int i = 2; i < triangles.Count;)
        {
            if (areVerticesSelected[triangles[i - 2]] && areVerticesSelected[triangles[i - 1]] && areVerticesSelected[triangles[i]])
            {/*
                int one = triangles[i - 2], two = triangles[i - 1], three = triangles[i];
                for (int x = 0; x < triangles.Count; x++)
                {
                    var num = triangles[x];
                    if (num > one) triangles[x]--;
                    if (num > two) triangles[x]--;
                    if (num > three) triangles[x]--;
                }*/
                deleted.Add(triangles[i - 2]);
                deleted.Add(triangles[i - 1]);
                deleted.Add(triangles[i]);/*
                vertices.RemoveAt(triangles[i - 2]);
                vertices.RemoveAt(triangles[i - 1] > triangles[i-2]? triangles[i-1]-1:triangles[i-1]);
                vertices.RemoveAt(triangles[i]>triangles[i-1]?(triangles[i]>triangles[i-2]?triangles[i]-2:triangles[i-1]-1):(triangles[i]>triangles[i-2]?triangles[i]-1:triangles[i]));
                if (uv.Count > triangles[i - 2]) uv.RemoveAt(triangles[i - 2]);
                if (uv.Count > triangles[i - 1]) uv.RemoveAt(triangles[i - 1] > triangles[i - 2] ? triangles[i - 1] - 1 : triangles[i - 1]);
                if (uv.Count > triangles[i]) uv.RemoveAt(triangles[i] > triangles[i - 1] ? (triangles[i] > triangles[i - 2] ? triangles[i] - 2 : triangles[i - 1] - 1) : (triangles[i] > triangles[i - 2] ? triangles[i] - 1 : triangles[i]));
                */
                triangles.RemoveRange(i - 2, 3);
                
            }
            else i += 3;
        }
        for (int x = vertices.Count-1; x >= 0 ; x--)
        {
            for (int y = 0; y < deleted.Count; y++)
            {
                if (x == deleted[y]) {
                    vertices.RemoveAt(x);
                    if (deleted[y] < uv.Count) uv.RemoveAt(deleted[y]);
                    if (vertices.Count == x) break;
                }
            }
        }
        for (int x = 0; x < triangles.Count; x++)
        {
            var num = triangles[x];
            for (int y = 0; y < deleted.Count; y++)
            {
                if (num > deleted[y]) triangles[x]--;
            }
        }

        selectedVertices.Clear();
        mesh.triangles = triangles.ToArray();
        mesh.vertices = vertices.ToArray();
        mesh.uv = uv.ToArray();
        RecalculateNormals();
    }
    
    public void DeleteVertices()
    {
        SaveSnapshot();
        var vertices = new List<Vector3>(mesh.vertices);
        var triangles = new List<int>(mesh.triangles);
        var uv = new List<Vector2>(mesh.uv);
        UpdateSelectedIndices();
        var deleted = new List<int>();
        for (int i = 2; i < triangles.Count;)
        {
            if (areVerticesSelected[triangles[i - 2]] || areVerticesSelected[triangles[i - 1]] || areVerticesSelected[triangles[i]])
            {
                deleted.Add(triangles[i - 2]);
                deleted.Add(triangles[i - 1]);
                deleted.Add(triangles[i]);
                triangles.RemoveRange(i - 2, 3);

            }
            else i += 3;
        }
        for (int x = vertices.Count - 1; x >= 0; x--)
        {
            for (int y = 0; y < deleted.Count; y++)
            {
                if (x == deleted[y])
                {
                    vertices.RemoveAt(x);
                    if (deleted[y] < uv.Count) uv.RemoveAt(deleted[y]);
                    if (vertices.Count == x) break;
                }
            }
        }
        for (int x = 0; x < triangles.Count; x++)
        {
            var num = triangles[x];
            for (int y = 0; y < deleted.Count; y++)
            {
                if (num > deleted[y]) triangles[x]--;
            }
        }

        selectedVertices.Clear();
        mesh.triangles = triangles.ToArray();
        mesh.vertices = vertices.ToArray();
        mesh.uv = uv.ToArray();
        RecalculateNormals();
    }

    public void SetUV(Vector2 coord) {
        SaveSnapshot();
        UpdateSelectedIndices();
        var oldUv = mesh.uv;
        var uv = new Vector2[areVerticesSelected.Length];
        var triangles = mesh.triangles;
        for (int i = 0; i < oldUv.Length; i++)
        {
            uv[i] = oldUv[i];
        }
        for (int i = 2; i < triangles.Length; i += 3)
        {
            if (areVerticesSelected[triangles[i - 2]] && areVerticesSelected[triangles[i - 1]] && areVerticesSelected[triangles[i]])
            {
                uv[triangles[i - 2]] = coord;
                uv[triangles[i - 1]] = coord;
                uv[triangles[i]] = coord;
            }
        }
        mesh.uv = uv;
    }

    void DeleteZeroSizeTriangles()
    {
        var vertices = new List<Vector3>(mesh.vertices);
        var triangles = new List<int>(mesh.triangles);
        var uv = new List<Vector2>(mesh.uv);
        UpdateSelectedIndices();
        var deleted = new List<int>();
        for (int i = 2; i < triangles.Count;)
        {
            if (vertices[triangles[i - 2]] == vertices[triangles[i-1]] || vertices[triangles[i - 2]] == vertices[triangles[i]] || vertices[triangles[i]] == vertices[triangles[i - 1]])
            {/*
                int one = triangles[i - 2], two = triangles[i - 1], three = triangles[i];
                for (int x = 0; x < triangles.Count; x++)
                {
                    var num = triangles[x];
                    if (num > one) triangles[x]--;
                    if (num > two) triangles[x]--;
                    if (num > three) triangles[x]--;
                }*/
                deleted.Add(triangles[i - 2]);
                deleted.Add(triangles[i - 1]);
                deleted.Add(triangles[i]);/*
                vertices.RemoveAt(triangles[i - 2]);
                vertices.RemoveAt(triangles[i - 1] > triangles[i-2]? triangles[i-1]-1:triangles[i-1]);
                vertices.RemoveAt(triangles[i]>triangles[i-1]?(triangles[i]>triangles[i-2]?triangles[i]-2:triangles[i-1]-1):(triangles[i]>triangles[i-2]?triangles[i]-1:triangles[i]));
                if (uv.Count > triangles[i - 2]) uv.RemoveAt(triangles[i - 2]);
                if (uv.Count > triangles[i - 1]) uv.RemoveAt(triangles[i - 1] > triangles[i - 2] ? triangles[i - 1] - 1 : triangles[i - 1]);
                if (uv.Count > triangles[i]) uv.RemoveAt(triangles[i] > triangles[i - 1] ? (triangles[i] > triangles[i - 2] ? triangles[i] - 2 : triangles[i - 1] - 1) : (triangles[i] > triangles[i - 2] ? triangles[i] - 1 : triangles[i]));
                */
                triangles.RemoveRange(i - 2, 3);

            }
            else i += 3;
        }
        for (int x = vertices.Count - 1; x >= 0; x--)
        {
            for (int y = 0; y < deleted.Count; y++)
            {
                if (x == deleted[y])
                {
                    vertices.RemoveAt(x);
                    if (deleted[y] < uv.Count) uv.RemoveAt(deleted[y]);
                    if (vertices.Count == x) break;
                }
            }
        }
        for (int x = 0; x < triangles.Count; x++)
        {
            var num = triangles[x];
            for (int y = 0; y < deleted.Count; y++)
            {
                if (num > deleted[y]) triangles[x]--;
            }
        }

        selectedVertices.Clear();
        mesh.triangles = triangles.ToArray();
        mesh.vertices = vertices.ToArray();
        mesh.uv = uv.ToArray();
        RecalculateNormals();
    }

    Vector3 SelectionMidPoint()
    {
        Vector3 midPoint = Vector3.zero;
        for (int i = 0; i < selectedVertices.Count; i++)
        {
            midPoint += selectedVertices[i];
        }
        midPoint /= selectedVertices.Count;
        return midPoint;
    }

    void JoinPoints()
    {
        SaveSnapshot();
        var vertices = mesh.vertices;
        var midPoint = SelectionMidPoint();
        for (int i = 0; i < selectedVertices.Count; i++)
        {
            for (int x = 0; x < vertices.Length; x++)
            {
                if (vertices[x] == selectedVertices[i])
                {
                    vertices[x] = midPoint;
                }
            }
        }
        mesh.vertices = vertices;
        selectedVertices.Clear();
        selectedVertices.Add(midPoint);
        DeleteZeroSizeTriangles();
    }

    public void BoxSelect(Vector3 mousePos1, Vector3 mousePos2)
    {
        var topLeft = new Vector3(mousePos1.x < mousePos2.x ? mousePos1.x : mousePos2.x, mousePos1.y < mousePos2.y ? mousePos1.y : mousePos2.y, 0);
        var botRight = new Vector3(mousePos1.x > mousePos2.x ? mousePos1.x : mousePos2.x, mousePos1.y > mousePos2.y ? mousePos1.y : mousePos2.y, 0);
        var vertices = mesh.vertices;
        for (int i = 0; i < vertices.Length; i++)
        {
            var pos = cam.WorldToScreenPoint(transform.position + vertices[i]);
            if (pos.x > topLeft.x && pos.x < botRight.x && pos.y > topLeft.y && pos.y < botRight.y && !selectedVertices.Contains(vertices[i])) selectedVertices.Add(vertices[i]);
        }
    }
    List<int>[] selectedVerticesIndices;
    bool[] areVerticesSelected;
    void UpdateSelectedIndices()
    {
        var vertices = mesh.vertices;
        selectedVerticesIndices = new List<int>[selectedVertices.Count];
        areVerticesSelected = new bool[vertices.Length];
        for (int y = 0; y < selectedVertices.Count; y++)
        {
            selectedVerticesIndices[y] = new List<int>();
            for (int x = 0; x < vertices.Length; x++)
            {
                if (vertices[x] == selectedVertices[y])
                {
                    selectedVerticesIndices[y].Add(x);
                    areVerticesSelected[x] = true;
                }
            }
        }
    }

    void StartGrab()
    {
        currentAction = Action.Grabbing;
        SaveSnapshot();
        grabPositions = new Vector3[selectedVertices.Count];
        for (int i = 0; i < grabPositions.Length; i++)
        {
            var ray = cam.ScreenPointToRay(Input.mousePosition);
            ray.origin -= transform.position;
            var d = -(drawer.cameraForward.x * selectedVertices[i].x + drawer.cameraForward.y * selectedVertices[i].y + drawer.cameraForward.z * selectedVertices[i].z);
            var t = -(d + ray.origin.x * drawer.cameraForward.x + ray.origin.y * drawer.cameraForward.y + ray.origin.z * drawer.cameraForward.z)
                    /
                (drawer.cameraForward.x * ray.direction.x + drawer.cameraForward.y * ray.direction.y + drawer.cameraForward.z * ray.direction.z);
            grabPositions[i] = new Vector3(ray.origin.x + t * ray.direction.x, ray.origin.y + t * ray.direction.y, ray.origin.z + t * ray.direction.z);
            if (alignToGrid) grabPositions[i] = new Vector3(Align(grabPositions[i].x), Align(grabPositions[i].y), Align(grabPositions[i].z));
        }
        UpdateSelectedIndices();
    }

    void Grab()
    {
        if (selectedVertices.Count == 0)
        {
            currentAction = Action.None;
            return;
        }
        var vertices = mesh.vertices;
        var dif = (Input.mousePosition - oldMousePos) * 6;
        dif.x /= Screen.width;
        dif.y /= Screen.height;
        for (int i = 0; i < selectedVertices.Count; i++)
        {

            var ray = cam.ScreenPointToRay(Input.mousePosition);
            ray.origin -= transform.position;
            var d = -(drawer.cameraForward.x * selectedVertices[i].x + drawer.cameraForward.y * selectedVertices[i].y + drawer.cameraForward.z * selectedVertices[i].z);
            var t = -(d + ray.origin.x * drawer.cameraForward.x + ray.origin.y * drawer.cameraForward.y + ray.origin.z * drawer.cameraForward.z)
                    /
                (drawer.cameraForward.x * ray.direction.x + drawer.cameraForward.y * ray.direction.y + drawer.cameraForward.z * ray.direction.z);
            var newPos = new Vector3(ray.origin.x + t * ray.direction.x, ray.origin.y + t * ray.direction.y, ray.origin.z + t * ray.direction.z);
            if (alignToGrid) newPos = new Vector3(Align(newPos.x), Align(newPos.y), Align(newPos.z));
            // ax + by +cz = -d  plane
            // a(x + d1*t) + b(y + d2*t) + c(z+ d3*t) = -d
            // a*x + d1*t*a + b*y + d2*t*b + c*z + d3*t*c = -d 
            // t(d1*a + d2*b + d3*c) = -d -a*x -b*y -c*z
            // t = -(d + a*x + b*y + c*z)/(d1*a + d2*b + d3*c)*/
            /*
            for (int p = 0; p < vertices.Length; p++)
            {
                if (vertices[p] == selectedVertices[i]) {
                    //vertices[p] = vertices[p] + cam.transform.rotation * dif;
                    vertices[p] = vertices[p] + newPos - grabPositions[i];
                }
            }*/
            var offset = newPos - grabPositions[i];
            for (int p = 0; p < selectedVerticesIndices[i].Count; p++)
            {
                
                vertices[selectedVerticesIndices[i][p]] = vertices[selectedVerticesIndices[i][p]] + new Vector3(currentAxis == Axis.All || (currentAxis&Axis.x)==Axis.x? offset.x :0,
                    currentAxis == Axis.All || (currentAxis & Axis.y) == Axis.y ? offset.y : 0,
                    currentAxis == Axis.All || (currentAxis & Axis.z) == Axis.z ? offset.z : 0
                    );
                if (alignToGrid) vertices[selectedVerticesIndices[i][p]] = new Vector3(Align(vertices[selectedVerticesIndices[i][p]].x),Align(vertices[selectedVerticesIndices[i][p]].y),Align(vertices[selectedVerticesIndices[i][p]].z));

            }
            //selectedVertices[i] = selectedVertices[i] + cam.transform.rotation * dif;
            selectedVertices[i] = selectedVertices[i] + new Vector3(currentAxis == Axis.All || (currentAxis & Axis.x) == Axis.x ? offset.x : 0,
                    currentAxis == Axis.All || (currentAxis & Axis.y) == Axis.y ? offset.y : 0,
                    currentAxis == Axis.All || (currentAxis & Axis.z) == Axis.z ? offset.z : 0
                    );
            grabPositions[i] = newPos;
            if (alignToGrid) grabPositions[i] = new Vector3(Align(grabPositions[i].x), Align(grabPositions[i].y), Align(grabPositions[i].z));
            if (alignToGrid) selectedVertices[i] = new Vector3(Align(selectedVertices[i].x), Align(selectedVertices[i].y), Align(selectedVertices[i].z));

        }
        mesh.vertices = vertices;
        RecalculateNormals();
    }

    public void SetSelectedXCoord(float f) {
        var vertices = mesh.vertices;
        for (int i = 0; i < vertices.Length; i++)
        {
            if(vertices[i] == selectedVertices[selectedVertices.Count - 1])
                vertices[i] = new Vector3(f, selectedVertices[selectedVertices.Count - 1].y, selectedVertices[selectedVertices.Count - 1].z);
        }
        selectedVertices[selectedVertices.Count - 1] = new Vector3(f, selectedVertices[selectedVertices.Count - 1].y, selectedVertices[selectedVertices.Count - 1].z);
        mesh.vertices = vertices;
    }
    public void SetSelectedYCoord(float f)
    {
        var vertices = mesh.vertices;
        for (int i = 0; i < vertices.Length; i++)
        {
            if (vertices[i] == selectedVertices[selectedVertices.Count - 1])
                vertices[i] = new Vector3(selectedVertices[selectedVertices.Count - 1].x, f, selectedVertices[selectedVertices.Count - 1].z);
        }
        selectedVertices[selectedVertices.Count - 1] = new Vector3(selectedVertices[selectedVertices.Count - 1].x, f, selectedVertices[selectedVertices.Count - 1].z);
        mesh.vertices = vertices;
    }
    public void SetSelectedZCoord(float f)
    {
        var vertices = mesh.vertices;
        for (int i = 0; i < vertices.Length; i++)
        {
            if (vertices[i] == selectedVertices[selectedVertices.Count - 1])
                vertices[i] = new Vector3(selectedVertices[selectedVertices.Count - 1].x, selectedVertices[selectedVertices.Count - 1].y, f);
        }
        selectedVertices[selectedVertices.Count - 1] = new Vector3(selectedVertices[selectedVertices.Count - 1].x, selectedVertices[selectedVertices.Count - 1].y, f);
        mesh.vertices = vertices;

    }

    float Align(float f) {
        var rem = (f % gridSize);
        float smaller = f - rem;
        float bigger = smaller + gridSize;

        if (rem > gridSize - rem) return bigger;
        else return smaller;
    }

    void RecalculateNormals()
    {
        var vertices = mesh.vertices;
        var triangles = mesh.triangles;
        var normals = new Vector3[vertices.Length];

        for (int i = 2; i < triangles.Length; i += 3)
        {
            var one = vertices[triangles[i - 2]] - vertices[triangles[i - 1]];
            var two = vertices[triangles[i - 2]] - vertices[triangles[i]];
            normals[triangles[i - 2]] = Vector3.Cross(one, two).normalized;
            normals[triangles[i - 1]] = Vector3.Cross(one, two).normalized;
            normals[triangles[i]] = Vector3.Cross(one, two).normalized;

        }
        mesh.normals = normals;
    }

    public void SaveMeshObj(string name) {       
        StringBuilder sb = new StringBuilder();
        var vertices = mesh.vertices;
        var normals = mesh.normals;
        var oldUv = mesh.uv;
        var uv = new Vector2[vertices.Length];
        for (int i = 0; i < oldUv.Length; i++)
        {
            uv[i] = oldUv[i];
        }
        var triangles = mesh.triangles;
        sb.AppendLine("o Mesh");
        for (int i = 0; i < vertices.Length; i++)
        {
            sb.AppendLine(string.Format("v {0} {1} {2}", vertices[i].x, vertices[i].y, vertices[i].z));
        }
        for (int i = 0; i < uv.Length; i++)
        {
            sb.AppendLine(string.Format("vt {0} {1}", uv[i].x, uv[i].y));
        }
        for (int i = 0; i < normals.Length; i++)
        {
            sb.AppendLine(string.Format("vn {0} {1} {2}", normals[i].x, normals[i].y, normals[i].z));
        }
        sb.Append("usemtl " + rend.sharedMaterial.name);
        sb.AppendLine();
        for (int i = 2; i < triangles.Length; i += 3)
        {
            sb.AppendLine(string.Format("f {0}/{0}/{0} {1}/{1}/{1} {2}/{2}/{2}", triangles[i - 2] + 1, triangles[i - 1] + 1, triangles[i] + 1));
        }

        File.WriteAllText(Application.streamingAssetsPath + "/" + name + ".obj", sb.ToString());
    }
    public void LoadMeshObj(string name) {
        
        SaveSnapshot();
        var lines = File.ReadAllLines(Application.streamingAssetsPath + "/" + name + ".obj");
        var vertices = new List<Vector3>();
        var preNormals = new List<Vector3>();
        var preUv = new List<Vector2>();
        var triangles = new List<int>();
        List<string> triLines = new List<string>();
        for (int i = 0; i < lines.Length; i++)
        {
            var args = lines[i].Split(' ');
            switch (args[0])
            {
                case ("v"):
                    vertices.Add(new Vector3(float.Parse(args[1]), float.Parse(args[2]), float.Parse(args[3])));
                    break;
                case ("vt"):
                    preUv.Add(new Vector2(float.Parse(args[1]), float.Parse(args[2])));
                    break;
                case ("vn"):
                    preNormals.Add(new Vector3(float.Parse(args[1]), float.Parse(args[2]), float.Parse(args[3])));
                    break;
                case ("f"): {
                        triLines.Add(args[1]);
                        triLines.Add(args[2]);
                        triLines.Add(args[3]);
                        break;
                    }
                default:
                    break;
            }
        }
        var normals = new Vector3[vertices.Count];
        var uv = new Vector2[vertices.Count];
        for (int i = 0; i < triLines.Count; i++)
        {
            string[] args = triLines[i].Split('/');
            triangles.Add(int.Parse(args[0]) - 1);
            uv[int.Parse(args[0]) - 1] = preUv[int.Parse(args[1]) - 1];
            normals[int.Parse(args[0]) - 1] = preNormals[int.Parse(args[2]) - 1];
        }
        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.uv = uv;
        mesh.normals = normals;
        
    }
    [System.Serializable]
    struct Save3 {
        public float x, y, z;
        public static implicit operator Vector3(Save3 s) {
            return new Vector3(s.x, s.y, s.z);
        }
        public static implicit operator Save3(Vector3 v) {
            return new Save3() { x = v.x, y = v.y, z = v.z };
        }

    }
    [System.Serializable]
    struct Save2
    {
        public float x, y;
        public static implicit operator Vector2(Save2 s)
        {
            return new Vector2(s.x, s.y);
        }
        public static implicit operator Save2(Vector2 v)
        {
            return new Save2() { x = v.x, y = v.y};
        }

    }
    [System.Serializable]
    struct SaveMesh {
        public Save3[] vertices;
        public int[] triangles;
        public Save3[] normals;
        public Save2[] uv;
    }
    public void SaveMeshBinary(string name) {
        SaveSnapshot();
        var m = snapshots[snapshots.Count - 1];
        var save = new SaveMesh() {vertices = Array.ConvertAll(m.vertices, item => (Save3)item),triangles = m.triangles, normals = Array.ConvertAll(m.normals, item => (Save3)item),uv = Array.ConvertAll(m.uv, item => (Save2)item) };
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.streamingAssetsPath + "/" + name + ".bin");
        bf.Serialize(file, save);
        file.Close();

    }
    public void LoadMeshBinary(string name) {
        SaveSnapshot();
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.streamingAssetsPath + "/" + name + ".bin", FileMode.Open);
        var m = (SaveMesh)bf.Deserialize(file);
        var save = new MeshGhost() { vertices = Array.ConvertAll(m.vertices, item => (Vector3)item), triangles = m.triangles, normals = Array.ConvertAll(m.normals, item => (Vector3)item), uv = Array.ConvertAll(m.uv, item => (Vector2)item), selectedVertices = new List<Vector3>() };
        snapshots.Add(save);
        LoadSnapshot(0);
        file.Close();

    }
}