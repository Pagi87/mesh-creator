﻿using UnityEngine;
using System.Collections;

public class Draw : MonoBehaviour {
    public MeshCreator creator;
    public Material circleMat;
    public Material lineMat;
    public Material helpLineMat;
    public Material boxSelectMat;
    public Material axisMat;
    public Vector3 cameraForward;
    public bool showWireframe = true;
    Camera cam;
    public int pointSizePixels = 5;
    Vector3 oldMousePos;
    public Vector3 cameraPivot = Vector3.zero;
    void Start() {
        cam = GetComponent<Camera>();
        cameraPivot = creator.transform.position;
    }

    void Update() {
        cameraForward = transform.forward;
        if (creator.allowInput)
        {
            if (Input.mouseScrollDelta.y != 0) Scroll();
            else if (Input.GetKey(KeyCode.LeftShift) && Input.GetMouseButtonDown(2)) StartShiftPivot();
            else if (Input.GetKey(KeyCode.LeftShift) && Input.GetMouseButton(2)) ShiftPivot();
            else if (Input.GetMouseButton(2)) RotateView();
            else if (Input.GetKeyDown(KeyCode.Alpha1)) LookFront(Input.GetKey(KeyCode.LeftControl));
            else if (Input.GetKeyDown(KeyCode.Alpha3)) LookSide(Input.GetKey(KeyCode.LeftControl));
            else if (Input.GetKeyDown(KeyCode.Alpha7)) LookTop(Input.GetKey(KeyCode.LeftControl));
            else if (Input.GetKeyDown(KeyCode.Alpha5)) SwitchCameraModes();
            else if (Input.GetKeyDown(KeyCode.B)) StartBoxSelect();
            else if (Input.GetKeyUp(KeyCode.B)) creator.BoxSelect(startBoxSelectMousePos, endBoxSelectMousePos);
        }
    }

    void LateUpdate() {
        oldMousePos = Input.mousePosition;
    }

    void OnPostRender() {
        DrawHelpLines();
        if (showWireframe)OutlineMesh();
        DrawPivot();
        DrawAxes();
        if (Input.GetKey(KeyCode.B)) DrawBoxSelect();
        if (creator.selectedVertices.Count == 0) return;
        DrawCircles();
    }
    Vector3 startBoxSelectMousePos;
    Vector3 endBoxSelectMousePos;
    void StartBoxSelect() {
        startBoxSelectMousePos = Input.mousePosition; 
    }
    void DrawBoxSelect() {
        GL.PushMatrix();
        boxSelectMat.SetPass(0);
        GL.LoadOrtho();
        GL.Begin(GL.QUADS);
        
        endBoxSelectMousePos = Input.mousePosition;
        startBoxSelectMousePos.z = 0;
        endBoxSelectMousePos.z = 0;
        if ((startBoxSelectMousePos.x > endBoxSelectMousePos.x && startBoxSelectMousePos.y > endBoxSelectMousePos.y) || (startBoxSelectMousePos.x < endBoxSelectMousePos.x && startBoxSelectMousePos.y < endBoxSelectMousePos.y))
        {
            GL.Vertex3(startBoxSelectMousePos.x / Screen.width, endBoxSelectMousePos.y / Screen.height, 0);
            GL.Vertex3(endBoxSelectMousePos.x / Screen.width, endBoxSelectMousePos.y / Screen.height, 0);
            GL.Vertex3(endBoxSelectMousePos.x / Screen.width, startBoxSelectMousePos.y / Screen.height, 0);
            GL.Vertex3(startBoxSelectMousePos.x / Screen.width, startBoxSelectMousePos.y / Screen.height, 0);            
        }
        else {
            GL.Vertex3(startBoxSelectMousePos.x / Screen.width, startBoxSelectMousePos.y / Screen.height, 0);
            GL.Vertex3(endBoxSelectMousePos.x / Screen.width, startBoxSelectMousePos.y / Screen.height, 0);
            GL.Vertex3(endBoxSelectMousePos.x / Screen.width, endBoxSelectMousePos.y / Screen.height, 0);
            GL.Vertex3(startBoxSelectMousePos.x / Screen.width, endBoxSelectMousePos.y / Screen.height, 0);
        }

        GL.End();
        GL.PopMatrix();
    }

    void DrawAxes() {
        GL.Begin(GL.LINES);
        axisMat.SetPass(0);
        if ((creator.currentAxis & MeshCreator.Axis.x) == MeshCreator.Axis.x) {
            GL.Vertex(new Vector3(-100, 0, 0) + cameraPivot);
            GL.Vertex(new Vector3(100, 0, 0) + cameraPivot);
        }
        if ((creator.currentAxis & MeshCreator.Axis.y) == MeshCreator.Axis.y)
        {
            GL.Vertex(new Vector3(0, -100, 0) + cameraPivot);
            GL.Vertex(new Vector3(0, 100, 0) + cameraPivot);
        }
        if ((creator.currentAxis & MeshCreator.Axis.z) == MeshCreator.Axis.z)
        {
            GL.Vertex(new Vector3(0, 0, -100) + cameraPivot);
            GL.Vertex(new Vector3(0, 0, 100) + cameraPivot);
        }
        GL.End();
    }

    void DrawPivot() {
        GL.Begin(GL.QUADS);
        helpLineMat.SetPass(0);
        GL.Vertex(cameraPivot + new Vector3(-0.1f,0, -0.1f));
        GL.Vertex(cameraPivot + new Vector3(-0.1f, 0, 0.1f));
        GL.Vertex(cameraPivot + new Vector3(0.1f, 0, 0.1f));
        GL.Vertex(cameraPivot + new Vector3(0.1f, 0, -0.1f));

        GL.Vertex(cameraPivot + new Vector3(0.1f, 0, -0.1f));
        GL.Vertex(cameraPivot + new Vector3(0.1f, 0, 0.1f));
        GL.Vertex(cameraPivot + new Vector3(-0.1f, 0, 0.1f));
        GL.Vertex(cameraPivot + new Vector3(-0.1f, 0, -0.1f));

        GL.End();
    }
    Vector3 mouseProjection;
    void StartShiftPivot() {
        var ray = cam.ScreenPointToRay(Input.mousePosition);
        var d = -(cameraForward.x * cameraPivot.x + cameraForward.y * cameraPivot.y + cameraForward.z * cameraPivot.z);
        var t = -(d + ray.origin.x * cameraForward.x + ray.origin.y * cameraForward.y + ray.origin.z * cameraForward.z)
                /
            (cameraForward.x * ray.direction.x + cameraForward.y * ray.direction.y + cameraForward.z * ray.direction.z);
        mouseProjection = new Vector3(ray.origin.x + t * ray.direction.x, ray.origin.y + t * ray.direction.y, ray.origin.z + t * ray.direction.z);
    }
    void ShiftPivot() {

        var ray = cam.ScreenPointToRay(Input.mousePosition);
        var d = -(cameraForward.x * cameraPivot.x + cameraForward.y * cameraPivot.y + cameraForward.z * cameraPivot.z);
        var t = -(d + ray.origin.x * cameraForward.x + ray.origin.y * cameraForward.y + ray.origin.z * cameraForward.z)
                /
            (cameraForward.x * ray.direction.x + cameraForward.y * ray.direction.y + cameraForward.z * ray.direction.z);
        var newPos = new Vector3(ray.origin.x + t * ray.direction.x, ray.origin.y + t * ray.direction.y, ray.origin.z + t * ray.direction.z);
        cameraPivot = cameraPivot - (newPos - mouseProjection)/2;
        transform.position = transform.position - (newPos - mouseProjection) / 2;
        mouseProjection = newPos;
    }

    void SwitchCameraModes() {
        if (cam.orthographic)
        {
            transform.position = -cam.orthographicSize * (transform.forward) + cameraPivot;
        }
        else {
            cam.orthographicSize = (transform.position - cameraPivot).magnitude;

        }
        cam.orthographic = !cam.orthographic;
    }

    void LookFront(bool opposite) {
        if (opposite)
        {
            var dist = (transform.position - cameraPivot).magnitude;
            transform.rotation = Quaternion.Euler(0, 180, 0);
            transform.position = new Vector3(cameraPivot.x, cameraPivot.y, cameraPivot.z + dist);
        }
        else {
            var dist = (transform.position - cameraPivot).magnitude;
            transform.rotation = Quaternion.Euler(0, 0, 0);
            transform.position = new Vector3(cameraPivot.x, cameraPivot.y, cameraPivot.z - dist);
        }
    }
    void LookSide(bool opposite)
    {
        if (opposite)
        {
            var dist = (transform.position - cameraPivot).magnitude;
            transform.rotation = Quaternion.Euler(0, 90, 0);
            transform.position = new Vector3(cameraPivot.x -dist, cameraPivot.y, cameraPivot.z);
        }
        else {
            var dist = (transform.position - cameraPivot).magnitude;
            transform.rotation = Quaternion.Euler(0, -90, 0);
            transform.position = new Vector3(cameraPivot.x + dist, cameraPivot.y, cameraPivot.z);
        }
    }
    void LookTop(bool opposite)
    {
        if (opposite)
        {
            var dist = (transform.position - cameraPivot).magnitude;
            transform.rotation = Quaternion.Euler(-90, 0, 0);
            transform.position = new Vector3(cameraPivot.x,cameraPivot.y -dist, cameraPivot.z);
        }
        else {
            var dist = (transform.position - cameraPivot).magnitude;
            transform.rotation = Quaternion.Euler(90, 0, 0);
            transform.position = new Vector3(cameraPivot.x, cameraPivot.y + dist, cameraPivot.z);
        }
    }

    void OutlineMesh() {
        if (creator.mesh != null && creator.mesh.vertexCount < 3) return;
        GL.Begin(GL.LINES);
        lineMat.SetPass(0);
        var vertices = creator.mesh.vertices;
        var triangles = creator.mesh.triangles;
        for (int i = 2; i < triangles.Length; i+=3)
        {
            if ((creator.transform.position + vertices[triangles[i - 2]] - transform.position).sqrMagnitude < 7000) {
                GL.Vertex(creator.transform.position + vertices[triangles[i - 2]]);
                GL.Vertex(creator.transform.position + vertices[triangles[i - 1]]);
            }

            if ((creator.transform.position + vertices[triangles[i]] - transform.position).sqrMagnitude < 7000)
            {
                GL.Vertex(creator.transform.position + vertices[triangles[i - 2]]);
                GL.Vertex(creator.transform.position + vertices[triangles[i]]);
            }
            if ((creator.transform.position + vertices[triangles[i - 1]] - transform.position).sqrMagnitude < 7000)
            {
                GL.Vertex(creator.transform.position + vertices[triangles[i - 1]]);
                GL.Vertex(creator.transform.position + vertices[triangles[i]]);
            }
        }
        GL.End();
    }

    void DrawHelpLines() {
        if ((creator.transform.position - transform.position).sqrMagnitude > 22500) return;
        GL.Begin(GL.LINES);
        helpLineMat.SetPass(0);
        for (int i = -10; i <= 10; i++)
        {
            GL.Vertex(creator.transform.position + new Vector3(-10, 0, i));
            GL.Vertex(creator.transform.position + new Vector3(10, 0, i));


            GL.Vertex(creator.transform.position + new Vector3(i, 0, -10));
            GL.Vertex(creator.transform.position + new Vector3(i, 0, 10));
        }

        GL.End();
    }

    void Scroll()
    {
        if (cam.orthographic)
        {
            cam.orthographicSize += -Input.mouseScrollDelta.y;
            if (cam.orthographicSize <= 0.1f) cam.orthographicSize = 0.11f;
        }
        else {
            transform.position += transform.forward * Input.mouseScrollDelta.y;
        }
    }
    void RotateView() {
        var rot = (Input.mousePosition - oldMousePos);/*
        rot.x /= Screen.width;
        rot.y /= Screen.height;*/
        transform.RotateAround(cameraPivot,Vector3.up , rot.x); // old transform.rotation*new Vector3(rot.y, -rot.x, 0)
        transform.RotateAround(cameraPivot, -transform.right, rot.y);
    }
    /*
    void DrawLines() {
        GL.PushMatrix();
        lineMat.SetPass(0);
        GL.LoadOrtho();
        GL.Begin(GL.LINES);
        GL.Color(Color.red);
        for (int i = 0; i < MeshCreator.selectedVertices.Count; i++) // nezaručuje že verticy jsou spojené v trianglu
        {
            var viewPoint = cam.WorldToViewportPoint(MeshCreator.selectedVertices[i - 1]);
            GL.Vertex(new Vector3(viewPoint.x, viewPoint.y, 0));
            var viewPoint2 = cam.WorldToViewportPoint(MeshCreator.selectedVertices[i]);
            GL.Vertex(new Vector3(viewPoint2.x, viewPoint2.y, 0));
        }
        GL.End();
        GL.PopMatrix();
    }
    */
    void DrawCircles() {
        GL.PushMatrix();
        circleMat.SetPass(0);
        GL.LoadOrtho();
        GL.Begin(GL.QUADS);
        for (int i = 0; i < creator.selectedVertices.Count; i++)
        {
            if (Vector3.SqrMagnitude(creator.transform.position + creator.selectedVertices[i] - transform.position) > 10000) continue;
            var viewPoint = cam.WorldToViewportPoint(creator.transform.position + creator.selectedVertices[i]);
            GL.TexCoord(new Vector3(0, 1, 0));
            GL.Vertex3(viewPoint.x - (float)pointSizePixels / Screen.width, viewPoint.y - (float)pointSizePixels / Screen.height, 0);
            GL.TexCoord(new Vector3(0, 0, 0));
            GL.Vertex3(viewPoint.x - (float)pointSizePixels / Screen.width, viewPoint.y + (float)pointSizePixels / Screen.height, 0);
            GL.TexCoord(new Vector3(1, 0, 0));
            GL.Vertex3(viewPoint.x + (float)pointSizePixels / Screen.width, viewPoint.y + (float)pointSizePixels / Screen.height, 0);
            GL.TexCoord(new Vector3(1, 1, 0));
            GL.Vertex3(viewPoint.x + (float)pointSizePixels / Screen.width, viewPoint.y - (float)pointSizePixels / Screen.height, 0);
        }
        GL.End();
        GL.PopMatrix();
    }
}
