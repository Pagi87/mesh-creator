﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ModellingUI : MonoBehaviour {
    public MeshCreator creator;
    public RectTransform deletePanel;
    public RectTransform menu;
    public GameObject menuBg;
    public Image paletteImage;

    public InputField  gridSize, saveName, loadName, iX, iY, iZ;
    public Text x, y, z;
    public Toggle snapGrid, wireframe; 

    public void ShowDeleteOptions() {
        deletePanel.anchoredPosition =new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        deletePanel.gameObject.SetActive(true);
    }

    void Update() {
        if (Input.GetMouseButtonDown(1)) deletePanel.gameObject.SetActive(false);
        if (Input.GetMouseButtonDown(0) && RectTransformUtility.RectangleContainsScreenPoint(paletteImage.rectTransform, Input.mousePosition)) ImageClick();
        else if (Input.GetKeyDown(KeyCode.Tab)) ToggleMenu();
    }

    public void DeleteTriangles() {
        deletePanel.gameObject.SetActive(false);
        creator.DeleteTriangles();
    }
    public void DeleteVertices() {
        deletePanel.gameObject.SetActive(false);
        creator.DeleteVertices();
    }

    public void ImageClick() {
        var localPoint = new Vector2();
        RectTransformUtility.ScreenPointToLocalPointInRectangle(paletteImage.rectTransform, Input.mousePosition, null, out localPoint);
        localPoint.x /= paletteImage.rectTransform.rect.width;
        localPoint.y /= paletteImage.rectTransform.rect.height;
        creator.SetUV(localPoint);
    }

    public void ShowVertexCoords(Vector3 v) {
        x.text = v.x.ToString();
        y.text = v.y.ToString();
        z.text = v.z.ToString();
    }
    public void ToggleWireframe(bool b) {
        creator.drawer.showWireframe = b;
    }
    public void ToggleSnap(bool b) {
        creator.alignToGrid = b;
    }
    public void SnapToGridSize(string s) {
        var f = float.Parse(s);
        creator.gridSize = f;
    }
    public void SetXCoord(string s) {
        var f = float.Parse(s);
        creator.SetSelectedXCoord(f);
        iX.text = "";
    }
    public void SetYCoord(string s)
    {
        var f = float.Parse(s);
        creator.SetSelectedYCoord(f);
        iY.text = "";
    }
    public void SetZCoord(string s)
    {
        var f = float.Parse(s);
        creator.SetSelectedZCoord(f);
        iZ.text = "";
    }
    public void Undo() {
        creator.LoadSnapshot(0);
    }
    public void SaveMeshObj() {
        if (!string.IsNullOrEmpty(saveName.text)) {
            creator.SaveMeshObj(saveName.text);
        }
    }

    public void SaveMeshBin()
    {
        if (!string.IsNullOrEmpty(saveName.text))
        {
            creator.SaveMeshBinary(saveName.text);
        }
    }
    public void LoadMeshObj() {
        if (!string.IsNullOrEmpty(loadName.text))
        {
            creator.LoadMeshObj(loadName.text);
        }
    }
    public void LoadMeshBin() {
        if (!string.IsNullOrEmpty(loadName.text))
        {
            creator.LoadMeshBinary(loadName.text);
        }
    }
    bool open = false;
    public void ToggleMenu() {
        if (open)
        {
            menu.position = new Vector3(-76.75f, menu.position.y, menu.position.z);
            menuBg.SetActive(false);
            creator.allowInput = true;
        }
        else {
            menu.position = new Vector3(76.75f, menu.position.y, menu.position.z);
            menuBg.SetActive(true);
            creator.allowInput = false;
        }
        open = !open;
    }
    public void GenerateQuad() {
        creator.GenerateQuad();
    }
    public void GenerateCube()
    {
        creator.GenerateCube();
    }
    public void GeneratePlane()
    {
        creator.GeneratePlane();
    }
}
